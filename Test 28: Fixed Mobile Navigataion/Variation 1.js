var height = jQuery('#header-wrapper .content-asset-container').innerHeight(),
    headerHeight = jQuery('#header-wrapper').innerHeight()

    jQuery('#header-wrapper').addClass('optFixIt');

jQuery(window).scroll(function () {
    var offset = jQuery(window).scrollTop()
    
    jQuery('#header-wrapper').css('top', -offset)
    jQuery('#main').css('margin-top', headerHeight-offset)

    if (offset > height) {
        jQuery('#header-wrapper, #main').addClass('optFixed');
    } else {
        jQuery('.optFixIt').removeClass('optFixed')
    }
})